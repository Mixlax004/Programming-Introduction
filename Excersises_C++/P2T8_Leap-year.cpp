#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <locale.h>

using namespace std;

// Headers
string toString (double);
int toInt (string);
double toDouble (string);

int main() {
	setlocale(LC_CTYPE, "Spanish");
    int a;
    
    cout << "Bienvenido al programa de identificaci�n de a�o bisiesto" << endl;
    cout << "por favor, ingrese el a�o que quiere identificar" << endl;
    cin >> a;
    if (a < 1582) {
        cout << "Los a�os bisiestos existen desde 1582, su a�o no es bisiesto" << endl;
    } else {
        if (a % 4 == 0 && a % 100 != 0) {
            cout << "El a�o " << a << " Es bisiesto" << endl;
        } else {
            if (a % 400 == 0) {
                cout << "El a�o " << a << " es bisiesto" << endl;
            } else {
                cout << "El a�o " << a << " No es bisiesto" << endl;
            }
        }
    }
    return 0;
}

// The following implements type conversion functions.
string toString (double value) { //int also
    stringstream temp;
    temp << value;
    return temp.str();
}

int toInt (string text) {
    return atoi(text.c_str());
}

double toDouble (string text) {
    return atof(text.c_str());
}

