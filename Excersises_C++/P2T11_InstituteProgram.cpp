#include <conio.h>
#include <stdio.h>
int AS, i, j, M, co;
int fe=0;
int ma=0;
int al=0;
float SA=0;
float PA, aux;
int main(){
	printf("Bienvenido al programa de rendimiento de los alumnos \nPor favor introduzca: \n");
	do{
	printf("a) Cantidad de alumnos: ");
	scanf("%d", &AS);
	if (AS>=100 || AS<0){
		printf("ERR: La cantidad de estudiantes debe ser un numero positivo de maximo 2 digitos, vuelva a ingresar. \n");
		}	
	}
	while (AS>=100 || AS<0);
	printf("b) Ingrese el codigo de los alumnos: \n");
	int CS[AS-1];
	for(i=0; i<AS; i++){
		do{
			printf("\nAlumno %d: ", i+1);
			scanf("%d", &CS[i]);
			if(CS[i]>=1000){
				printf("ERR: El codigo de los estudiantes debe ser un numero de maximo 3 digitos, vuelva a ingresar. \n");
			}
		}
		while(CS[i]>=1000);
	}
	printf("c) Ingrese el sexo de los alumnos: \n");
	int SS[AS-1];
	for(i=0; i<AS; i++){
		do{
			printf("\nIngrese 0 para femenino o 1 para masculino. Alumno %d: ", i+1);
			scanf("%d", &SS[i]);
			if(SS[i]!=1 && SS[i]!=0){
				printf("ERR: el sexo de los estudiantes debe ser 0 o 1, vuelva a ingresar. \n");
			}
		}
		while(SS[i]!=1 && SS[i]!=0);
	}
	printf("d) Ingrese la nota de algebra de los alumnos: \n");
	float NA[AS-1];
	for(i=0; i<AS; i++){
		do{
			printf("\nIngrese la nota del Alumno %d: ", i+1);
			scanf("%g", &NA[i]);
			if(NA[i]>5){
				printf("ERR: la nota maxima es de 5.0, vuelva a ingresar. \n");
			}
		}
		while(NA[i]>5);
	}
	printf("e) Ingrese la nota de fundamentos de programacion de los alumnos: \n");
	float NF[AS-1];
	for(i=0; i<AS; i++){
		do{
			printf("\nIngrese la nota del Alumno %d: ", i+1);
			scanf("%g", &NF[i]);
			if(NF[i]>5){
				printf("ERR: la nota maxima es de 5.0, vuelva a ingresar. \n");
			}
		}
		while(NF[i]>5);
	}
	if(AS!=0){
		do{
	do{
		printf("\n\nQue informacion desea ver? \n1. Promedio de algebra \n2. Mayor nota de Fundamentos de programacion \n3. Cantidad de alumnos femeninos y masculinos \n4. Buscar notas de un codigo en especifico \n5. Salir\n");
		scanf("%d", &M);
		if(M>5 || M<0){
			printf("\nERR: el valor ingresado no es valido, vuelva a ingresar. \n");
		}
	}
	while(M>5 || M<0);
	if(M==1){
		for(i=0; i<AS; i++)
		SA=SA+NA[i];
	PA=SA/AS;
	printf("\nEl promedio de los estudiantes en algebra es: %g", PA);
	}
	if(M==2){
		for(i=0; i<AS; i++){
			for(j=0; j<AS-1; j++){
				if(NF[j]<NF[j+1]){
					aux=NF[j];
					NF[j]=NF[j+1];
					NF[j+1]=aux;
				}
			}
		}
	printf("\nLa nota mas alta de fundamentos de programacion es: %g", NF[0]);
	}
	if(M==3){
		for(i=0; i<AS; i++){
			if(SS[i]==0){
				fe++;
			}
			else{
				ma++;
			}
		}
	printf("\nHay %d alumno(s) femeninos", fe);
	printf("\nHay %d alumno(s) masculinos", ma);
	}
	if(M==4){
		do{
			printf("Ingrese el codigo del estudiante cuyas notas desea imprimir: \n");
			scanf("%d", &co);
			if(co>=1000){
				printf("ERR: El codigo de los estudiantes debe ser un numero de maximo 3 digitos, vuelva a ingresar. \n");
			}
		}
		while(co>=1000);
		
		for(i=0; i<AS; i++){
			if(CS[i]==co){
				printf("La nota de algebra del estudiante de codigo %d es: ", co);
				printf("%g", NA[i]);
				printf("\nLa nota de fundamentos de programacion del estudiante de codigo %d es: ", co);
				printf("%g", NF[i]);
				al=1;
			}
		}
		if(al==0){
			printf("ERR: El codigo ingresado no coincide con ningun estudiante registrado");
		}
		al=0;
	}	
	}
	while(M!=5);
	}
	else{
		printf("\nNo hay alumnos sobre los que trabajar");
	}
}
