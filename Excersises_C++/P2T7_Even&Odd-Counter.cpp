#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <locale.h>

using namespace std;

// Headers
string toString (double);
int toInt (string);
double toDouble (string);

int main() {
	setlocale(LC_ALL, "");
    double countS, countF;
    int i, oddC, evenC;
    
    oddC = 0;
    evenC = 0;
    cout << "Bienvenido al contador de n�meros pares e impares" << endl;
    cout << "Por favor ingrese el valor en el que desea que inicie su contador" << endl;
    cin >> countS;
    while (std::fmod(countS,1) != 0) {
        cout << "Ha ingresado un valor no entero, el programa solo funciona con valores enteros" << endl;
        cout << "Por favor ingrese el valor en el que desea que inicie su contador" << endl;
        cin >> countS;
    }
    cout << "Por favor ingrese el valor en el que desea que finalice su contador" << endl;
    cin >> countF;
    while (std::fmod(countF,1) != 0) {
        cout << "Ha ingresado un valor no entero, el programa solo funciona con valores enteros" << endl;
        cout << "Por favor ingrese el valor en el que desea que finalice su contador" << endl;
        cin >> countF;
    }
    if (countS < countF) {
        for (i = countS; i <= countF; i++) {
            
            // Ahora evaluamos si el n�mero i es par o impar
            if (std::fmod(i,2)!= 0) {
                cout << i << " es un n�mero impar" << endl;
                oddC = oddC + 1;
            } else {
                cout << i << " es un n�mero par" << endl;
                evenC = evenC + 1;
            }
        }
    } else {
        for (i = countF; i <= countS; i++) {
            
            // Ahora evaluamos si el n�mero i es par o impar
            if (std::fmod(i,2)!= 0) {
                cout << i << " es un n�mero impar" << endl;
                oddC = oddC + 1;
            } else {
                cout << i << " es un n�mero par" << endl;
                evenC = evenC + 1;
            }
        }
    }
    cout << "Hay un total de " << evenC << " n�meros pares y " << oddC << " n�meros impares" << endl;
    return 0;
}

// The following implements type conversion functions.
string toString (double value) { //int also
    stringstream temp;
    temp << value;
    return temp.str();
}

int toInt (string text) {
    return atoi(text.c_str());
}

double toDouble (string text) {
    return atof(text.c_str());
}

