#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <locale.h>

using namespace std;

// Headers
string toString (double);
int toInt (string);
double toDouble (string);

int main() {
	setlocale(LC_ALL, "");
    double a, b, c, d, e;
    double f, i;
    
    f = 2;
    cout << "Bienvenido al programa multitareas" << endl;
    cout << "Por favor ingrese el n�mero acorde a una de las siguientes opciones:  1. Tabla de multiplicar  2. Ordenar tres n�meros " << endl;
    cin >> a;
    for (i = 1; i <= f; i++) {
        if (a == 1 || a == 2) {
        } else {
            cout << "ERR: valor inv�lido detectado.  Por favor ingrese el n�mero acorde a una de las siguientes opciones:  1. Tabla de multiplicar  2. Ordenar tres n�meros " << endl;
            cin >> a;
            f = f + 1;
        }
    }
    if (a == 1) {
        cout << "Ha selecionado crear una tabla de multiplicar.  Por favor ingrese el n�mero inicial de la tabla: " << endl;
        cin >> b;
        cout << "Por favor ingrese el n�mero final de la tabla: " << endl;
        cin >> c;
        cout << "Por favor ingrese el n�mero patr�n de la tabla: " << endl;
        cin >> d;
        if (b < c) {
            for (i = b; i <= c; i++) {
                e = d * i;
                cout << d << "x" << i << "=" << e << endl;
            }
        } else {
            for (i = c; i <= b; i++) {
                e = d * i;
                cout << d << "x" << i << "=" << e << endl;
            }
        }
    } else {
        cout << "Ha selecionado ordenar tres n�meros.  Por favor ingrese el primer n�mero: " << endl;
        cin >> b;
        cout << "Por favor ingrese el segundo n�mero: " << endl;
        cin >> c;
        cout << "Por favor ingrese el tercer n�mero: " << endl;
        cin >> d;
        if (b == c || c == d || b == d) {
            if (b == c) {
                if (c == d) {
                    cout << "Los tres n�meros son iguales" << endl;
                } else {
                    if (d > b) {
                        cout << d << "es el mayor de los n�meros, le siguen: " << b << " y " << c << ", los cuales son iguales" << endl;
                    } else {
                        cout << b << " y " << c << " son los mayores n�meros, son iguales. le sigue: " << d << endl;
                    }
                }
            } else {
                if (b == d) {
                    if (c > b) {
                        cout << c << "es el mayor de los n�meros, le siguen: " << b << " y " << d << ", los cuales son iguales" << endl;
                    } else {
                        cout << b << " y " << d << " son los mayores n�meros, son iguales. le sigue: " << c << endl;
                    }
                } else {
                    if (b > c) {
                        cout << b << "es el mayor de los n�meros, le siguen: " << c << " y " << d << ", los cuales son iguales" << endl;
                    } else {
                        cout << c << " y " << d << " son los mayores n�meros, son iguales. le sigue: " << b << endl;
                    }
                }
            }
        } else {
            if (b > c && b > d) {
                if (c > d) {
                    cout << "Los n�meros est�n ordenados de mayor a menor: " << b << "_" << c << "_" << d << endl;
                } else {
                    cout << "Los n�meros est�n ordenados de mayor a menor: " << b << "_" << d << "_" << c << endl;
                }
            } else {
                if (c > d && c > b) {
                    if (b > d) {
                        cout << "Los n�meros est�n ordenados de mayor a menor: " << c << "_" << b << "_" << d << endl;
                    } else {
                        cout << "Los n�meros est�n ordenados de mayor a menor: " << c << "_" << d << "_" << b << endl;
                    }
                } else {
                    if (b > c) {
                        cout << "Los n�meros est�n ordenados de mayor a menor: " << d << "_" << b << "_" << c << endl;
                    } else {
                        cout << "Los n�meros est�n ordenados de mayor a menor: " << d << "_" << c << "_" << b << endl;
                    }
                }
            }
        }
    }
    return 0;
}

// The following implements type conversion functions.
string toString (double value) { //int also
    stringstream temp;
    temp << value;
    return temp.str();
}

int toInt (string text) {
    return atoi(text.c_str());
}

double toDouble (string text) {
    return atof(text.c_str());
}

