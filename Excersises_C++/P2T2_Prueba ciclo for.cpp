#include <stdio.h> //a�adimos librerias
#include <conio.h>
#include <locale.h>
float i, A;//declaramos variables

int main(void) {
	setlocale(LC_ALL, ""); //usamos este comando para que nuestro programa reconozca caracteres con t�ldes entre otros.
	printf("Por favor ingrese el n�mero en el que desea que se inicie el ciclo: ");
	scanf("%d",&i);
	
	do{ 
		printf("Por favor ingrese el n�mero en el que desea que se detenga el ciclo, \nrecuerde que debe ser un n�mero mayor que el anterior\n"); // Solicitamos el valor que ser� el fin de nuestro ciclo
		scanf("%d",&A);
		if(A<i){
			printf("\nPor favor ingrese un valor v�lido\n"); //le indicamos que ha hecho mal el ingreso de los datos.
		}
	}
	while(A<i); //realizamos este do-while para evitar que el usuario ingrese un valor menor que el primero como valor final.
	
	for(i;i<=A;i++){ //establecemos as condiciones del "for"; el valor inicial del contador, la condic�n para que se siga ejecutando el ciclo, y la taza de cambio de contador respectivamente. 
		printf("%d",i); //mediante esta impresi�n podemos ver el comportamiento de "i" en cada ciclo.
		printf("\n");//a�adimos este espaciado para colocar cada impresi�n en una l�nea diferente.
	}
}

