#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cmath>
#include <locale.h>

using namespace std;

// Headers
string toString (double);
int toInt (string);
double toDouble (string);

int main() {
	setlocale(LC_ALL, "");
    double a, b;
    
    cout << "Bienvenido al programa de determinar par o impar.  Por favor ingrese el n�mero que quiere escanear" << endl;
    cin >> a;
    b = std::fmod(a,1);
    if (b != 0) {
        cout << "No ha ingresado un n�mero entero, el valor que ingres� no ser� par ni impar" << endl;
    } else {
        b = std::fmod(b,2);
        if (b == 0) {
            cout << "El n�mero ingresado " << a << " es par" << endl;
        } else {
            cout << "El n�mero ingresado " << a << " es impar" << endl;
        }
    }
    return 0;
}

// The following implements type conversion functions.
string toString (double value) { //int also
    stringstream temp;
    temp << value;
    return temp.str();
}

int toInt (string text) {
    return atoi(text.c_str());
}

double toDouble (string text) {
    return atof(text.c_str());
}

